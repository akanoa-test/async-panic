extern crate core;

use futures::executor::block_on;
use futures::FutureExt;
use std::future::Future;
use std::panic;
use std::panic::{AssertUnwindSafe, UnwindSafe};

async fn handler<T, O, T1, O1>(closure: O, cleaning: O1)
where
    O: Future<Output = T> + UnwindSafe,
    O1: Future<Output = T1> + UnwindSafe,
    //F: FnOnce() -> O,
{
    println!("----------Avant");
    let result = AssertUnwindSafe(closure).catch_unwind().await;

    cleaning.await;
    println!("----------Après");

    if let Err(err) = result {
        panic::resume_unwind(err)
    }
}

#[test]
fn run() {
    block_on(async {
        handler(
            async {
                will_panic(1).await;
                will_panic(1).await;
            },
            async { println!("Nettoyage") },
        )
        .await;
    });
}

async fn will_panic(a: u8) {
    println!("will panic");
    assert_eq!(1, a, "0 not equal 1")
}

fn main() {}
